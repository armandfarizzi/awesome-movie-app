const router = require("express").Router();

//  import controller collection
const movie = require("./controllers/movieController");
const user = require("./controllers/userController");

//  import application middleware
const auth = require("./middlewares/authentication");
const successResponse = require("./middlewares/successResponse");
const fileHandler = require("./middlewares/fileHandler");

// Movie Collection API
router.get("/movies/:id?", movie.find, successResponse);

// Movie Review API
router.post("/movies/:id/review", auth, movie.review, successResponse);
router.put("/review/:id", auth, movie.updateReview, successResponse);
router.get(
  "/movies/:id/review/tag/:tag_id",
  movie.showReviewByTags,
  successResponse
);

// User Collection API
router.post("/register", user.register, successResponse);
router.post("/login", user.login, successResponse);
router.get("/me", auth, user.me, successResponse);
router.get("/user/verify", user.verify, successResponse);
router.get("/user/sendVerify", auth, user.sendVerify, successResponse);
router.put("/user", auth, user.update, successResponse);
router.post(
  "/user/avatar",
  auth,
  fileHandler.single("image"),
  user.uploadAvatar,
  successResponse
);
module.exports = router;
