const request = require("supertest");
const app = require("../server");
const { User } = require("../models");

describe("Root endpoint test", () => {
  const deleteAllCollection = async function () {
    await User.destroy({
      truncate: true,
      restartIdentity: true,
    });
  };
  beforeAll(async () => {
    await deleteAllCollection();
  });

  it("Should succes to register a new account", async (done) => {
    const res = await request(app).post("/api/v1/register").send({
      email: "armandfarizzi@gmail.com",
      name: "armand",
      username: "armandf",
      password: "12345678",
    });
    expect(res).toBeDefined();
    expect(res.body.data.name).toBe("armand");
    expect(res.body.data.access_token).toBeDefined();
    expect(res.statusCode).toBe(201);
    done();
  });
  it("Should failed to register a account with same email", async (done) => {
    const res = await request(app).post("/api/v1/register").send({
      email: "armandfarizzi@gmail.com",
      name: "armand",
      username: "armandf",
      password: "12345678",
    });

    expect(res.statusCode).toBe(422);
    expect(res.body.message[0]).toMatch("Email");
    done();
  });
  it("Should succes to login with registered account ", async (done) => {
    const res = await request(app).post("/api/v1/login").send({
      email: "armandfarizzi@gmail.com",
      password: "12345678",
    });
    console.log(res.body);
    expect(res.body).toBeDefined();
    expect(res.body.data.access_token).toBeDefined();
    expect(res.statusCode).toBe(200);
    done();
  });
  it("Should failed to login with unregistered ", async (done) => {
    const res = await request(app).post("/api/v1/login").send({
      email: "invalid@gmail.com",
      password: "12345678",
    });
    expect(res.body).toBeDefined();
    expect(res.statusCode).toBe(400);
    done();
  });
  it("Should failed to login with unmatch password", async (done) => {
    const res = await request(app).post("/api/v1/login").send({
      email: "armandfarizzi@gmail.com",
      password: "invalid",
    });
    expect(res.body).toBeDefined();
    expect(res.statusCode).toBe(401);
    done();
  });
});
