const handlerError = (err, req, res, next) => {
  if (req.statusCode == 200) {
    req.status(500);
  }
  // console.log(err);
  res.json({ status: "failed", message: [err.message] });
};
module.exports = handlerError;
