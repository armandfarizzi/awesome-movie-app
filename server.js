require("dotenv").config();
const express = require("express");
const morgan = require("morgan");
const router = require("./router");
const cors = require("cors");
const adminRouter = require("./adminRouter");
const swaggerUi = require("swagger-ui-express");
const YAML = require("yamljs");
const swaggerDocument = YAML.load("./swagger.yaml");
const app = express();

app.use(cors());
app.use(express.json());
if (process.env.NODE_ENV != "test") app.use(morgan("dev"));

app.get("/", (req, res) =>
  res.status(200).json({
    status: "success",
    message: "Hello World",
  })
);

app.use("/docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use("/api/admin", adminRouter);
app.use("/api/v1", router);

const handlerError = require("./middlewares/handlerError");

app.use(handlerError);

// const Movie = require("./models/movie");
// const Review = require("./models/review");
// const deleteOnce = async () => {
//   await Movie.deleteMany();
//   await Review.deleteMany();
// };
// deleteOnce();

module.exports = app;
