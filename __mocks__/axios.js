const faker = require("faker");
// const axios = require("axios");

const axios = jest.genMockFromModule("axios");
const fakerObj = function () {
  return {
    data: {
      title: faker.name.title(),
      overview: faker.lorem.paragraph(),
      poster_path: faker.image.imageUrl(),
      genres: [
        {
          id: 16,
          name: "Animation",
        },
        {
          id: 10751,
          name: "Family",
        },
      ],
    },
  };
};

const obj = [];
for (let i = 0; i < 20; i++) {
  // const element = [i];
  obj.push(fakerObj());
}
axios.get.mockResolvedValue(obj[Math.floor(Math.random() * obj.length)]);

module.exports = axios;
