const sgMail = require("@sendgrid/mail");
const User = require("../models/user");
require("dotenv").config();

sgMail.setApiKey(process.env.SENDGRID_API_KEY);

const message = (doc) => ({
  to: `${doc.email}`,
  from: "armandfarizzi@gmail.com",
  subject: `Authentication link for user ${doc.name}`,
  text: "and easy to do anywhere, even with Node.js",
  html: `<strong>and easy to do anywhere, even with Node.js</strong>
    <a href='${process.env.BASE_URL}/api/v1/user/verify?token=${doc.getToken(
    "5m"
  )}'>verify user</a>
  `,
});

module.exports = async function (doc) {
  try {
    await sgMail.send(message(doc));
    // console.log(
    //   `link to authenticate user :\n${
    //     process.env.BASE_URL
    //   }user/verify?token=${doc.getToken("1m")}`
    // );
  } catch (error) {
    console.log(error.response.body.errors);
  }
};
