"use strict";
const { detailMovie } = require("../lib/queryMovieData");

module.exports = (sequelize, DataTypes) => {
  const Movie = sequelize.define(
    "Movie",
    {
      title: DataTypes.STRING,
      tmdb_id: DataTypes.INTEGER,
      image_url: DataTypes.STRING,
      synopsis: DataTypes.TEXT,
    },
    {}
  );
  Movie.associate = function (models) {
    Movie.hasMany(models.Review);
  };
  Object.defineProperty(Movie, "getter", {
    get() {
      return {
        title: this.title,
        tmdb_id: this.tmdb_id,
        poster: this.image_url,
        synopsis: this.synopsis,
      };
    },
  });
  return Movie;
};
