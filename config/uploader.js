const path = require("path");
const multer = require("multer");
require("dotenv").config();

module.exports = {
  development: {
    storage: multer.diskStorage({
      destination: function (req, file, cb) {
        cb(null, path.resolve(__dirname, "..", "public", "uploads"));
      },
      filename: function (req, file, cb) {
        const split = file.originalname.split(".");
        const ext = split[split.length - 1];
        const name = `${file.fieldname}-${Date.now()}.${ext}`;
        file.url = `${process.env.BASE_URL}/uploads/${name}`;

        cb(null, name);
      },
    }),
  },
  test: {},
  production: {},
};