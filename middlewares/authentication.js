const { User } = require("../models");
const jwt = require("jsonwebtoken");
require("dotenv").config();

const auth = async (req, res, next) => {
  if (!req.headers.authorization) {
    res.statusCode = 401;
    next(new Error("Not authenticated"));
    return;
  }
  try {
    const payload = jwt.verify(req.headers.authorization, process.env.JWT_KEY);
    const foundUser = await User.findByPk(payload.id);
    if (!foundUser) {
      res.statusCode = 404;
      return next(new Error("User not found"));
    }
    res.user = foundUser;
    return next();
  } catch (error) {
    res.statusCode = 400;
    next(error);
  }
};
module.exports = auth;
