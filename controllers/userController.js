const { User } = require("../models");
const jwt = require("jsonwebtoken");
const sendVerif = require("../lib/sendVerificationMail");
const ImageKit = require("../lib/imagekit");

module.exports = {
  async register(req, res, next) {
    try {
      const newUser = await User.create(req.body);
      res.statusCode = 201;
      res.data = newUser.entity;
      next();
    } catch (error) {
      res.statusCode = 422;
      next(error);
    }
  },
  async login(req, res, next) {
    try {
      const thisUser = await User.findOne({ where: { email: req.body.email } });
      if (thisUser.checkCredentials(req.body.password)) {
        res.data = thisUser.entity;
        return next();
      }
      res.statusCode = 401;
      next(new Error("Credentials does not match"));
    } catch (error) {
      res.statusCode = 400;
      next(error);
    }
  },
  async logout(req, res) {
    res.status(200).json({
      status: "success",
      message: "logout",
    });
  },
  async me(req, res, next) {
    console.log(res.user.verified);
    res.data = res.user.entity;
    next();
  },
  async verify(req, res, next) {
    try {
      const token = req.query.token;
      const { email } = jwt.verify(token, process.env.JWT_KEY);
      console.log(email);
      const foundUser = await User.updateOne({ email }, { verified: true });
      res.json({ status: "success", data: foundUser });
    } catch (error) {
      res.statusCode = 403;
      next(error);
    }
  },
  async sendVerify(req, res, next) {
    const isVerified = res.user.verified;
    if (!isVerified) {
      sendVerif(res.user);
      res.data = "Email verification has been sent!";
      return next();
    }
    res.data = "You already verified!";
    next();
  },
  async update(req, res, next) {
    try {
      const blacklist = ["email", "password", "id"];
      for (let keys in req.body) {
        if (blacklist.indexOf(keys) == -1) {
          res.user[keys] = req.body[keys];
        }
      }
      await res.user.save();
      res.data = res.user.entity;
      next();
    } catch (error) {
      res.status(400);
      next(error);
    }
  },
  async uploadAvatar(req, res, next) {
    if (process.env.NODE_ENV !== "production") {
      res.user.image_url = "http://" + req.file.url;
      await res.user.save();
      res.data = res.user;
      next();
      return;
    }
    try {
      const upload = await ImageKit.upload({
        file: req.file.buffer,
        fileName: req.file.originalname,
      });
      res.user.image_url = upload.url;
      await res.user.save();
      res.data = upload;
      next();
    } catch (error) {
      res.statusCode = 422;
      next(error);
    }
  },
};
