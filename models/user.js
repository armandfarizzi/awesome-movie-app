"use strict";
const bcrypt = require("bcryptjs");
const gravatar = require("gravatar");
const jwt = require("jsonwebtoken");
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    "User",
    {
      name: DataTypes.STRING,
      username: DataTypes.STRING,
      email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: {
          args: true,
          msg: "Email has already been taken",
        },
        validate: {
          isEmail: {
            args: true,
            msg: "Invalid email",
          },
        },
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          len: {
            args: 8,
            msg: "Password must be at least 8 characters",
          },
        },
      },
      image_url: {
        type: DataTypes.TEXT,
        validate: {
          isUrl: {
            args: true,
            msg: "Invalid url for user's image_url",
          },
        },
      },
      verified: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
      role: {
        type: DataTypes.ENUM,
        values: ["user", "admin"],
        defaultValue: "user",
      },
    },
    {
      // defaultScope: {
      //   attributes: {
      //     exclude: ["updatedAt", "createdAt", "password", "role", "verified"],
      //   },
      // },
      scopes: {
        nocredentials: {
          attributes: {
            exclude: ["updatedAt", "createdAt", "password", "role", "verified"],
          },
        },
      },
      hooks: {
        beforeCreate: function (instance) {
          const encrypted_password = bcrypt.hashSync(
            instance.password,
            bcrypt.genSaltSync(10)
          );
          instance.password = encrypted_password;
          instance.image_url = gravatar.url(
            instance.email,
            { s: "100", r: "x" },
            true
          );
        },
      },
    }
  );
  User.associate = function (models) {
    // associations can be defined here
    User.hasMany(models.Review);
  };
  Object.defineProperty(User.prototype, "entity", {
    get() {
      return {
        id: this.id,
        email: this.email,
        name: this.name,
        username: this.username,
        image_url: this.image_url,

        access_token: this.getToken(),
      };
    },
  });
  User.prototype.getToken = function () {
    return jwt.sign(
      {
        id: this.id,
        email: this.email,
        image_url: this.image_url,
      },
      process.env.JWT_KEY,
      { expiresIn: "1d" }
    );
  };
  User.prototype.checkCredentials = function (password) {
    return bcrypt.compareSync(password, this.password);
  };
  return User;
};
