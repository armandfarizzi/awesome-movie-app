"use strict";
module.exports = (sequelize, DataTypes) => {
  const Review_Tag = sequelize.define(
    "Review_Tag",
    {
      TagId: DataTypes.INTEGER,
      ReviewId: DataTypes.INTEGER,
    },
    {
      defaultScope: {
        attributes: { exclude: ["updatedAt", "createdAt"] },
      },
    }
  );
  Review_Tag.associate = function (models) {
    // associations can be defined here
  };
  return Review_Tag;
};
