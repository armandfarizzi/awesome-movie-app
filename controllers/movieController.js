// const axios = require("axios");
/*
 * Search Endpoint
 */

const { Movie, Review, User, Tag, Review_Tag } = require("../models");
const { Op } = require("sequelize");
module.exports = {
  async find(req, res, next) {
    if (req.params.id) {
      try {
        const showMovie = await Movie.findByPk(req.params.id, {
          attributes: {
            exclude: ["updatedAt", "createdAt"],
          },
          include: {
            model: Review,
            attributes: {
              exclude: ["updatedAt", "createdAt"],
            },
            include: [
              {
                model: User,
                attributes: {
                  exclude: [
                    "password",
                    "role",
                    "updatedAt",
                    "createdAt",
                    "verified",
                    "email",
                  ],
                },
              },
              {
                model: Tag,
                as: "tags",
              },
            ],
          },
        });
        if (!showMovie) {
          res.statusCode = 404;
          next(new Error("Movie not found"));
        }
        res.data = showMovie;
        return next();
      } catch (error) {
        res.statusCode = 400;
        return next(error);
      }
    }
    try {
      const page = req.query.page || 1;
      const pagination = { limit: 5, offset: (page - 1) * 5 };
      const { count, rows } = await Movie.findAndCountAll({
        ...pagination,
      });
      res.data = rows;
      res.metadata = {
        countAll: count,
        first: `${process.env.BASE_URL}movies`,
        last: `${process.env.BASE_URL}movies?page=${Math.ceil(count / 5)}`,
        next:
          page * 5 < count
            ? `${process.env.BASE_URL}movies?page=${Number(page) + 1}`
            : null,
        previous:
          page > 1 ? `${process.env.BASE_URL}movies?page=${page - 1}` : null,
      };
      next();
    } catch (error) {
      res.statusCode = 400;
      next(error);
    }
  },

  async review(req, res, next) {
    try {
      /**
       *  since each user can not to give a review twice, find the user
       *  review to the movieId given, if review is found, then throw
       *  to a fail response.
       */
      const isAlreadyReview = await Review.findOne({
        where: { UserId: res.user.id, MovieId: req.params.id },
      });
      if (isAlreadyReview) {
        res.statusCode = 400;
        return next(new Error("You already post a review to this movies"));
      }

      /**
       *  1. Create the review of the movie first,
       *  2. loop the tagsArr, create and append to review immediately
       */
      const newReview = await Review.create({
        ...req.body,
        MovieId: req.params.id,
        UserId: res.user.id,
      });

      try {
        /**
         *  req.body.tags is expected to having a format of
         *  string as "tags1, tags2, tags3", so i need to parse them
         *  into an array by using split() method with ", " as a divider
         *
         *  return value ["tags1", "tags2", "tags3"]
         */
        const tagsArr = req.body.tags.split(", ");

        for (let i = 0; i < tagsArr.length; i++) {
          const tagToAdded = await Tag.findOrCreate({
            where: { name: tagsArr[i].toLowerCase() },
            defaults: { name: tagsArr[i].toLowerCase() },
          });

          await newReview.addTag(tagToAdded[0], { through: Review_Tag });
        }
      } catch (error) {
        // console.log(req.body.tags);
        // console.error(error.message);
      }

      // const response = await Review.findOne({
      //   where: {
      //     id: newReview.id,
      //   },
      //   include: {
      //     model: Tag,
      //     as: "tags",
      //   },
      // });

      const response = await Movie.findOne({
        where: { id: req.params.id },
        attributes: {
          exclude: ["updatedAt", "createdAt"],
        },
        include: {
          model: Review,
          attributes: {
            exclude: ["updatedAt", "createdAt"],
          },
          include: [
            {
              model: User,
              attributes: {
                exclude: [
                  "password",
                  "role",
                  "updatedAt",
                  "createdAt",
                  "verified",
                ],
              },
            },
            {
              model: Tag,
              as: "tags",
            },
          ],
        },
      });

      res.data = response;
      res.statusCode = 201;
      next();
    } catch (error) {
      res.statusCode = 400;
      next(error);
    }
  },

  async updateReview(req, res, next) {
    try {
      const { comment, rating } = req.body;
      const { tags } = req.body;
      const updateReview = await Review.update(
        {
          comment,
          rating,
        },
        {
          where: {
            id: req.params.id,
            UserId: res.user.id,
          },
        }
      );
      const reviewInstance = await Review.findOne({
        where: { id: req.params.id, UserId: res.user.id },
        include: { model: Tag, as: "tags" },
      });
      if (!updateReview[0]) {
        res.statusCode = 404;
        return next(new Error("Review either not found or it's not yours"));
      }
      if (tags) {
        const tagsArr = tags.split(", ");
        await reviewInstance.setTags([]);
        for (let i = 0; i < tagsArr.length; i++) {
          const tagToAdded = await Tag.findOrCreate({
            where: { name: tagsArr[i].toLowerCase() },
            defaults: { name: tagsArr[i].toLowerCase() },
          });

          await reviewInstance.addTag(tagToAdded[0], { through: Review_Tag });
        }
      } else if (!tags) {
        await reviewInstance.setTags([]);
      }

      res.message = "review is updated";
      next();
    } catch (error) {
      res.statusCode = 400;
      next(error);
    }
  },
  async showReviewByTags(req, res, next) {
    try {
      const data = await Review.findAll({
        include: [
          {
            model: Tag,
            as: "tags",
          },
        ],
        where: {
          "$tags.id$": { [Op.eq]: req.params.tag_id },
          MovieId: req.params.id,
        },
      });
      let reviewArray = [];
      for (let i = 0; i < data.length; i++) {
        const append = await Review.findByPk(data[i].id, {
          include: [
            { model: Tag, as: "tags" },
            {
              model: User,
              attributes: {
                exclude: [
                  "updatedAt",
                  "createdAt",
                  "password",
                  "role",
                  "verified",
                ],
              },
            },
          ],
        });
        reviewArray.push(append);
      }
      res.data = reviewArray;
      next();
    } catch (error) {
      res.statusCode = 400;
      next(error);
    }
  },
};
