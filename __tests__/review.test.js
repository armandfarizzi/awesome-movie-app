const request = require("supertest");
const app = require("../server");
const util = require("util");

const { Movie, User, Review, Tag, Review_Tag } = require("../models");
jest.mock("axios");
require("dotenv").config();
let log = function (any) {
  console.log(util.inspect(any, { showHidden: false, depth: null }));
};

describe("Movie API test case collections", () => {
  let usertoken1,
    usertoken2,
    usertoken3,
    usertoken4,
    movieid1,
    movieid2,
    reviewid1;
  beforeAll(async () => {
    const deleteAllCollection = async function () {
      await Review_Tag.destroy({
        truncate: true,
        restartIdentity: true,
      });
      await Movie.destroy({
        truncate: true,
        restartIdentity: true,
      });
      await Review.destroy({
        truncate: true,
        restartIdentity: true,
      });
      await User.destroy({
        truncate: true,
        restartIdentity: true,
      });
      await Tag.destroy({
        truncate: true,
        restartIdentity: true,
      });
      usertoken1 = (
        await User.create({
          name: "user1",
          username: "user1",
          email: "user1@mail.com",
          password: "12345678",
        })
      ).getToken();
      usertoken2 = (
        await User.create({
          name: "user2",
          username: "user2",
          email: "user2@mail.com",
          password: "12345678",
        })
      ).getToken();
      usertoken3 = (
        await User.create({
          name: "user3",
          username: "user3",
          email: "user3@mail.com",
          password: "12345678",
        })
      ).getToken();
      usertoken4 = (
        await User.create({
          name: "user4",
          username: "user4",
          email: "user4@mail.com",
          password: "12345678",
        })
      ).getToken();
      for (let i = 1; i <= 15; i++) {
        await request(app)
          .post("/api/admin/movies/" + i)
          .set("Authorization", usertoken1)
          .set("admin_key", process.env.ADMIN_KEY);
      }
    };
    await deleteAllCollection();
    jest.mock("../lib/queryMovieData");
  });
  let reviewId0;

  it("Should successfully give review and write the inserted the tags", async (done) => {
    const res = await request(app)
      .post(`/api/v1/movies/1/review`)
      .set("authorization", usertoken1)
      .send({
        comment: "very good..",
        rating: 4,
        tags: "Funny, dark",
      });
    expect(res.statusCode).toBe(201);
    expect(res.body.data.Reviews[0].tags.length).toBe(2);
    reviewId0 = res.body.data.Reviews[0].id;
    done();
  });

  let reviewId1;

  it("Should successfully give review without insert the tags", async (done) => {
    const res = await request(app)
      .post(`/api/v1/movies/1/review`)
      .set("authorization", usertoken2)
      .send({
        comment: "I love it",
        rating: 4,
      });
    expect(res.statusCode).toBe(201);
    expect(res.body.data.Reviews.length).toBe(2);

    expect(res.body.data.Reviews[1].tags.length).toBe(0);
    reviewId1 = res.body.data.Reviews[1].id;
    done();
  });

  let reviewId2;
  it("Should successfully reuse the registered tags", async (done) => {
    const res = await request(app)
      .post(`/api/v1/movies/1/review`)
      .set("authorization", usertoken3)
      .send({
        comment: "It so good",
        rating: 4,
        tags: "DaRK",
      });
    await request(app)
      .post(`/api/v1/movies/1/review`)
      .set("authorization", usertoken4)
      .send({
        comment: "dark knight too dark",
        rating: 4,
        tags: "Mystery, DaRK",
      });
    await request(app)
      .post(`/api/v1/movies/3/review`)
      .set("authorization", usertoken4)
      .send({
        comment: "dark too but romantic",
        rating: 4,
        tags: "Mystery, DaRK, romantic",
      });
    expect(res.statusCode).toBe(201);
    expect(res.body.data.Reviews.length).toBe(3);
    reviewId2 = res.body.data.Reviews[1].id;
    done();
  });
  it("'tags' must be included in movie/:id endpoints", async (done) => {
    const res = await request(app).get(`/api/v1/movies/1`);
    expect(res.statusCode).toBe(200);
    expect(res.body.data.Reviews[0].tags.length).toBe(2);
    done();
  });
  it("Should success to update review", async (done) => {
    expect(reviewId0).toBe(1);
    expect(reviewId1).toBe(2);
    expect(reviewId2).toBe(3);
    const log1 = await Review.findByPk(reviewId0, {
      include: { model: Tag, as: "tags" },
    });
    // console.log(log1.tags[0].toJSON(), log1.tags[1].toJSON());
    const update0 = await request(app)
      .put(`/api/v1/review/${reviewId0}`)
      .set("authorization", usertoken1)
      .send({
        rating: 3,
        comment: "updated comment",
        tags: "updated tags, dark, mystery",
      });

    expect(update0.body.message).toMatch("updated");
    const log2 = await Review.findByPk(reviewId0, {
      include: { model: Tag, as: "tags" },
    });
    // console.log(
    //   log2.tags[0].toJSON(),
    //   log2.tags[1].toJSON(),
    //   log2.tags[2].toJSON()
    // );
    done();
  });
  it("Should success to update review - II", async (done) => {
    expect(reviewId0).toBe(1);
    expect(reviewId1).toBe(2);
    expect(reviewId2).toBe(3);

    const update0 = await request(app)
      .put(`/api/v1/review/${reviewId0}`)
      .set("authorization", usertoken1)
      .send({
        rating: 3,
        comment: "updated comment -- II ",
        tags: "updated tags, new tags, dark",
      });

    expect(update0.body.message).toMatch("updated");

    done();
  });
  it("Should fail to update review if UserId from review not equal to id from token", async (done) => {
    expect(reviewId0).toBe(1);
    expect(reviewId1).toBe(2);
    expect(reviewId2).toBe(3);

    const update0 = await request(app)
      .put(`/api/v1/review/${reviewId0}`)
      .set("authorization", usertoken2)
      .send({ rating: 3, comment: "updated comment" });

    expect(update0.body.message[0]).toMatch("found");
    done();
  });
  it("Should fail to update review if review id is not found or invalid", async (done) => {
    expect(reviewId0).toBe(1);
    expect(reviewId1).toBe(2);
    expect(reviewId2).toBe(3);

    const update0 = await request(app)
      .put(`/api/v1/review/11`)
      .set("authorization", usertoken2)
      .send({ rating: 3, comment: "updated comment" });

    expect(update0.body.message[0]).toMatch("found");
    done();
  });
  /**
   */
  it('Should return all reviews that having "dark" (tag id is equal to 2) as their tags and in movies with MovieId equal to 1', async (done) => {
    const res = await request(app).get("/api/v1/movies/1/review/tag/2");
    res.body.data.forEach((testreview) => {
      expect(testreview.tags.some((tags) => tags.id == 2)).toBe(true);

      expect(testreview.MovieId).toBe(1);
    });
    done();
  });
});
