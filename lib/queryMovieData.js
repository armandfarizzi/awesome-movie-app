const axios = require("axios");

const TMDB_KEY = `api_key=${process.env.TMDB_KEY}`;
const TMDB_URL = `https://api.themoviedb.org/3/`;

/**
 * return movie with ID
 */
const searchMovie = async function (query, page) {
  const response = await axios.get(
    `${TMDB_URL}search/movie?query=${query}&page=${page}&${TMDB_KEY}`
  );
  // console.log()
  return response;
};

const detailMovie = async function (movie_id) {
  //   console.log(response?);
  const response = await axios.get(`${TMDB_URL}movie/${movie_id}?${TMDB_KEY}`);
  // console.log(response.data);

  return response.data;
};

module.exports = {
  searchMovie,
  detailMovie,
};
