/**
 * this mocks somewhat doesn't works
 */

const dataMovie = {
  12: {
    title: "Finding Memo",
    overview:
      "Nemo, an adventurous young clownfish, is unexpectedly taken from his Great Barrier Reef home to a dentist's office aquarium. It's up to his worrisome father Marlin and a friendly but forgetful fish Dory to bring Nemo home -- meeting vegetarian sharks, surfer dude turtles, hypnotic jellyfish, hungry seagulls, and more along the way.",
    poster_path: "/xVNSgrsvpcAHPnyKf2phYxyppNZ.jpg",
    genres: [
      {
        id: 16,
        name: "Animation",
      },
      {
        id: 10751,
        name: "Family",
      },
    ],
  },
  15: {
    title: "Citizen Kane",
    overview:
      "Newspaper magnate, Charles Foster Kane is taken from his mother as a boy and made the ward of a rich industrialist. As a result, every well-meaning, tyrannical or self-destructive move he makes for the rest of his life appears in some way to be a reaction to that deeply wounding event.",
    poster_path: "/sav0jxhqiH0bPr2vZFU0Kjt2nZL.jpg",
    genres: [
      {
        id: 9648,
        name: "Mystery",
      },
      {
        id: 18,
        name: "Drama",
      },
    ],
  },
};

const detailMovie = (id) => {
  return jest.fn().mockReturnValue(Promise.resolve(dataMovie[id]));
};

module.exports = { detailMovie };
