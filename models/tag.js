"use strict";
module.exports = (sequelize, DataTypes) => {
  const Tag = sequelize.define(
    "Tag",
    {
      name: DataTypes.STRING,
    },
    {
      defaultScope: {
        attributes: { exclude: ["updatedAt", "createdAt"] },
      },
    }
  );
  Tag.associate = function (models) {
    // associations can be defined here
    Tag.belongsToMany(models.Review, { as: "reviews", through: "Review_Tag" });
  };
  return Tag;
};
