const request = require("supertest");
const app = require("../server");

describe("Root endpoint test", () => {
  it("should success to requesting root endpoint", async (done) => {
    const res = await request(app).get("/");
    expect(res).toBeDefined();
    expect(res.statusCode).toBe(200);
    done();
  });
});
