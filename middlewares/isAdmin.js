require("dotenv").config();

const isAdmin = (req, res, next) => {
  // console.log(req.headers.admin_key == process.env.ADMIN_KEY);
  if (req.headers.admin_key == process.env.ADMIN_KEY) {
    return next();
  }

  res.statusCode = 403;

  return next(new Error("Forbidden"));
};

module.exports = isAdmin;
