const router = require("express").Router();

// const movie = require("./controllers/movieController");
const adminMovie = require("./controllers/adminMovieController");

// const user = require("./controllers/userController");

const auth = require("./middlewares/authentication");
const successResponse = require("./middlewares/successResponse");
const isAdmin = require("./middlewares/isAdmin");
// /**
//  *  Base Path   : localhost:8000/admin/
//  *  required    : Authentication token
//  *
//  * */

router.post(
  "/movies/:movie_id",
  auth,
  isAdmin,
  adminMovie.new,
  successResponse
);

router.delete(
  "/movies/:movie_id",
  auth,
  isAdmin,
  adminMovie.delete,
  successResponse
);

module.exports = router;
