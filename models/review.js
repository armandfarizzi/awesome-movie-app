"use strict";
module.exports = (sequelize, DataTypes) => {
  const Review = sequelize.define(
    "Review",
    {
      comment: {
        type: DataTypes.TEXT,
        allowNull: false,
      },
      UserId: DataTypes.INTEGER,
      MovieId: DataTypes.INTEGER,
      rating: {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
          //   notEmpty: {
          //     args: true,
          //     msg: "Rating can not be empty",
          //   },
          max: {
            args: 5,
            msg: "rating can not more than 5",
          },
          min: {
            args: 1,
            msg: "rating can not less than 1",
          },
        },
      },
    },
    {
      defaultScope: {
        attributes: { exclude: ["updatedAt", "createdAt"] },
      },
    }
  );
  Review.associate = function (models) {
    // associations can be defined here
    Review.belongsTo(models.User);
    Review.belongsTo(models.Movie);
    Review.belongsToMany(models.Tag, { as: "tags", through: "Review_Tag" });
  };

  return Review;
};
