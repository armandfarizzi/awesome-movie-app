module.exports = function (req, res, next) {
  res
    .status(res.statusCode || 200)
    .json({
      status: "success",
      data: res.data,
      metadata: res.metadata,
      message: res.message,
    });
};
