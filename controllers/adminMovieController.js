const { Movie } = require("../models");

const { searchMovie, detailMovie } = require("../lib/queryMovieData");

module.exports = {
  async find(req, res, next) {
    try {
      const page = req.query.page || 1;
      const fetchData = await searchMovie(req.query.keyword, page);
      res.data = fetchData.data.results;
      res.metadata = {};
      next();
    } catch (error) {
      res.statusCode = 400;
      next(error);
    }
  },
  async delete(req, res, next) {
    try {
      await Movie.destroy({ where: { id: req.params.movie_id } });
      res.message = `Movie #${req.params.movie_id} has been deleted`;
      next();
    } catch (error) {
      res.statusCode = 400;
      next(error);
    }
  },
  async show(req, res, next) {
    try {
      // const fetchData = await
    } catch (error) {}
  },
  async new(req, res, next) {
    try {
      const isMovieAlreadyExist = await Movie.findOne({
        where: { tmdb_id: req.params.movie_id },
      });
      if (isMovieAlreadyExist) {
        res.statusCode = 400;
        return next(new Error("Movie already exist in database!"));
      }
      const result = await detailMovie(req.params.movie_id);

      const { title, genres, id, overview, poster_path } = result;
      const createdEntries = await Movie.create({
        title,
        synopsis: overview,
        image_url: "http://image.tmdb.org/t/p/w500" + poster_path,
        tmdb_id: req.params.movie_id,
      });
      res.data = createdEntries;
      res.statusCode = 201;
      next();
    } catch (error) {
      console.log(error);
      res.status(400).json({ status: "failed", message: error.message });
    }
  },
};
