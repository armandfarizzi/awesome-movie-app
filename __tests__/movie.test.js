const request = require("supertest");
const app = require("../server");
const { Movie, User, Review } = require("../models");
jest.mock("axios");
require("dotenv").config();

describe("Movie API test case collections", () => {
  let usertoken1, usertoken2, movieid1, movieid2, reviewid1;
  beforeAll(async () => {
    const deleteAllCollection = async function () {
      await Movie.destroy({
        truncate: true,
        restartIdentity: true,
      });
      await Review.destroy({
        truncate: true,
        restartIdentity: true,
      });
      await User.destroy({
        truncate: true,
        restartIdentity: true,
      });
      usertoken1 = (
        await User.create({
          name: "user1",
          username: "user1",
          email: "user1@mail.com",
          password: "12345678",
        })
      ).getToken();
      usertoken2 = (
        await User.create({
          name: "user2",
          username: "user2",
          email: "user2@mail.com",
          password: "12345678",
        })
      ).getToken();
    };
    await deleteAllCollection();
    jest.mock("../lib/queryMovieData");
  });

  it("userToken1 and userToken2 should be defined to be integrated with next case", () => {
    expect(usertoken1).toBeDefined();
    expect(usertoken2).toBeDefined();
    expect(process.env.ADMIN_KEY).toBeDefined();
  });
  it("should fail to add movie if not authenticated", async (done) => {
    const res = await request(app).post("/api/admin/movies/12");
    expect(res.statusCode).toBe(401);
    expect(res.body).toBeDefined();
    done();
  });
  it("should fail to add movie if authenticated but no bypass key in headers", async (done) => {
    const res = await request(app)
      .post("/api/admin/movies/12")
      .set("Authorization", usertoken1);
    expect(res.body.message[0]).toBe("Forbidden");
    expect(res.statusCode).toBe(403);
    done();
  });
  it("should success to add movie if authenticated and inserting bypass key in headers", async (done) => {
    const res = await request(app)
      .post("/api/admin/movies/12")
      .set("Authorization", usertoken1)
      .set("admin_key", process.env.ADMIN_KEY);
    const res2 = await request(app)
      .post("/api/admin/movies/15")
      .set("Authorization", usertoken1)
      .set("admin_key", process.env.ADMIN_KEY);
    expect(res.statusCode).toBe(201);
    expect(res.body.data.title).toBeDefined();
    expect(res.body.data.tmdb_id).toBe(12);
    movieid1 = res.body.data.id;
    movieid2 = res2.body.data.id;
    done();
  });
  it("should fail to add movie if movie already exist", async (done) => {
    const res = await request(app)
      .post("/api/admin/movies/12")
      .set("Authorization", usertoken1)
      .set("admin_key", process.env.ADMIN_KEY);
    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBeDefined();
    done();
  });
  it("should success to query a movie", async (done) => {
    const res = await request(app).get(`/api/v1/movies/${movieid1}`);
    expect(res.body.data.id).toBe(movieid1);
    expect(res.statusCode).toBe(200);
    done();
  });
  it("should fail to post a movie review if not authenticated", async (done) => {
    const res = await request(app)
      .post(`/api/v1/movies/${movieid1}/review`)
      .send({ comment: "very good..", rating: 4 });
    expect(res.statusCode).toBe(401);
    expect(res.body.message[0]).toMatch("authenticated");
    done();
  });
  it("should fail to post a movie review if having a bad request body", async (done) => {
    const res = await request(app)
      .post(`/api/v1/movies/${movieid1}/review`)
      .set("authorization", usertoken1)
      .send({ commenttt: "very good..", rating: 4 });
    expect(res.statusCode).toBe(400);
    expect(res.body.message[0]).toMatch("comment");
    done();
  });
  it("should fail to post a movie review if having a rating more than 5", async (done) => {
    const res = await request(app)
      .post(`/api/v1/movies/${movieid1}/review`)
      .set("authorization", usertoken1)
      .send({ comment: "very good..", rating: 6 });
    expect(res.statusCode).toBe(400);
    expect(res.body.message[0]).toMatch("5");
    done();
  });
  it("should success to post a movie review", async (done) => {
    const res = await request(app)
      .post(`/api/v1/movies/${movieid1}/review`)
      .set("authorization", usertoken1)
      .send({ comment: "very good..", rating: 4 });
    await request(app)
      .post(`/api/v1/movies/${movieid1}/review`)
      .set("authorization", usertoken2)
      .send({ comment: "This very bad", rating: 1 });
    expect(res.statusCode).toBe(201);
    expect(res.body.data.Reviews).toBeDefined();
    expect(res.body.data).toBeDefined();
    done();
  });
  it("a member should fail to post a movie review twice ", async (done) => {
    const res = await request(app)
      .post(`/api/v1/movies/${movieid1}/review`)
      .set("authorization", usertoken1)
      .send({ comment: "very good..", rating: 4 });

    expect(res.statusCode).toBe(400);
    expect(res.body.message).toBeDefined();
    done();
  });
  it("should success to query a movie with comment inserted", async (done) => {
    const res = await request(app).get(`/api/v1/movies/${movieid1}`);
    expect(res.body.data.id).toBe(movieid1);
    expect(res.body.data.Reviews.length).toBe(2);
    expect(res.body.data.Reviews[0].User.image_url).toBeDefined();
    expect(res.statusCode).toBe(200);
    done();
  });
  it("Movie with no comment will have zero length of reviews", async (done) => {
    const res = await request(app).get(`/api/v1/movies/${movieid2}`);
    expect(res.body.data.id).toBe(movieid2);
    expect(res.body.data.Reviews.length).toBe(0);
    expect(res.statusCode).toBe(200);
    done();
  });
  it("Should success return movies maximal 5 movies each", async (done) => {
    for (let i = 0; i < 15; i++) {
      await request(app)
        .post("/api/admin/movies/" + i * 4)
        .set("Authorization", usertoken1)
        .set("admin_key", process.env.ADMIN_KEY);
    }
    const res = await request(app).get(`/api/v1/movies?page=2`);
    // console.log(res.body);
    expect(res.body.data.length).toBe(5);
    done();
  });
  it("will success delete if auth", async (done) => {
    const delRes = await request(app)
      .delete("/api/admin/movies/" + 1)
      .set("Authorization", usertoken1)
      .set("admin_key", process.env.ADMIN_KEY);
    expect(delRes.body.message).toMatch("deleted");
    const res = await request(app).get(`/api/v1/movies/1`);
    expect(res.body.message[0]).toMatch("found");
    done();
  });
});
